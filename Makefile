# $Id$

.PHONY: all
all:
	cd src && $(MAKE) all

.PHONY: install
install:
	cd src && $(MAKE) install

.PHONY: clean
clean:
	cd src && $(MAKE) clean
