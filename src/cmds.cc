/* $Id$ */

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include "wouxun.h"
#include "impexp.h"
#include "throw.h"
#include "intl.h"
#include "file.h"
#include "cmds.h"
#include "csv.h"

#define MEMORY_SIZE 0x2000

static void RejectOptions(const CCli &cli, const std::string forbidden)
{
	for (std::string::const_iterator i(forbidden.begin()); i != forbidden.end(); ++i)
		if (cli.Avail(*i))
			Throw(_("Option \"-%c\" not available with this command. See help"), *i);
}

static std::string GetEnv(const std::string &name)
{
	const char *env(getenv(name.c_str()));
	return env ? env : "";
}

static void PrepareWouxun(const CCli &cli, CWouxun &wx)
{
	std::string port(GetEnv("OWX_PORT"));

	if (cli.Avail('a'))
		Throw(_("Port autodetection not working yet. Sorry :)"));

	if (cli.Avail('p'))
		port = cli.Param('p');

	if (port.empty())
		Throw(_("Port not specified. See help"));

	wx.SetPort(port);

	int timeout(0);
	bool set(false);

	const std::string env(GetEnv("OWX_TIMEOUT"));
	if (!env.empty())
	{
		timeout = atoi(env.c_str());
		set = true;
	}

	if (cli.Avail('t'))
	{
		timeout = atoi(cli.Param('t').c_str());
		set = true;
	}

	if (set)
	{
		if (timeout < 0)
			Throw(_("Invalid timeout specified. Must be positive"));

		wx.SetTimeout(timeout);
	}

	wx.Open();

	printf(_("Found radio: %s\n"), wx.GetIDString().c_str());

	if (wx.GetIDString() != "KG669V")
	{
		if (cli.Avail('f'))
			printf(_("Warning: This radio was not tested and may be not supported!\n"));
		else
			Throw(_("This radio is not supported. See help"));
	}
}

void CmdUnknown(const CCli & /* cli */)
{
	Throw(_("Bad command or command not specified. See help"));
}

void CmdCheck(const CCli &cli)
{
	RejectOptions(cli, "ior");

	CWouxun wx;
	PrepareWouxun(cli, wx);
}

void CmdGet(const CCli &cli)
{
	RejectOptions(cli, "ir");

	if (!cli.Avail('o'))
		Throw(_("Binary file to write to not specified, use -o"));

	CFileWrite f;
	f.Open(cli.Param('o').c_str());

	CWouxun wx;
	PrepareWouxun(cli, wx);

	char buf[MEMORY_SIZE];
	for (size_t i(0); i < sizeof(buf); i += 0x40)
	{
		printf(_("Reading address 0x%04X (%u%% done)\r"), i, i * 100 / sizeof(buf));
		fflush(stdout);
		wx.GetPage(i, buf + i);
	}

	printf(_("\n"));

	f.Write(buf, sizeof(buf));
	f.Close();
}

static void LoadBinFile(const std::string &path, char *buf, size_t sz)
{
	CFileRead f;
	f.Open(path.c_str());

	if (f.Size() != sz)
		Throw(_("%s: Invalid file size"), path.c_str());

	if (f.Read(buf, sz) != sz)
		Throw(_("%s: Error reading"), path.c_str());
}

void CmdPut(const CCli &cli)
{
	RejectOptions(cli, "o");

	if (!cli.Avail('i'))
		Throw(_("Binary file to read from not specified, use -i"));

	if (!cli.Avail('r'))
		printf(_("Consider using -r\n"));

	char buf[MEMORY_SIZE];
	LoadBinFile(cli.Param('i'), buf, sizeof(buf));

	bool useref(cli.Avail('r'));
	char refbuf[sizeof(buf)];

	if (useref)
		LoadBinFile(cli.Param('r'), refbuf, sizeof(refbuf));

	CWouxun wx;
	PrepareWouxun(cli, wx);

	for (size_t i(0); i < sizeof(buf); i += 0x10)
	{
		bool skip(false);
		if (useref && !memcmp(buf + i, refbuf + i, 0x10))
			skip = true;

		printf(_("%s address 0x%04X (%u%% done) \r"), skip ? _("Skipping") : _("Writing"), i, i * 100 / sizeof(buf));
		fflush(stdout);

		if (!skip)
			wx.PutPage(i, buf + i);
	}

	printf(_("\n"));
}

void CmdExport(const CCli &cli)
{
	RejectOptions(cli, "fptr");

	if (!cli.Avail('i'))
		Throw(_("Binary file to export from was not specified"));

	if (!cli.Avail('o'))
		Throw(_("CSV file to export to was not specified"));

	char buf[MEMORY_SIZE];
	LoadBinFile(cli.Param('i'), buf, sizeof(buf));

	CCsv csv(ROWS, COLS);

	Export(csv, buf, sizeof(buf));

	csv.Save(cli.Param('o'));
}

void CmdImport(const CCli &cli)
{
	RejectOptions(cli, "fptr");

	if (!cli.Avail('i'))
		Throw(_("CSV file to import from was not specified"));

	if (!cli.Avail('o'))
		Throw(_("Binary file to import to was not specified"));

	CCsv csv(ROWS, COLS);
	csv.Load(cli.Param('i'));

	char buf[MEMORY_SIZE];
	LoadBinFile(cli.Param('o'), buf, sizeof(buf));

	Import(buf, sizeof(buf), csv);

	CFileWrite f;
	f.Open(cli.Param('o').c_str());
	f.Write(buf, sizeof(buf));
	f.Close();
}
