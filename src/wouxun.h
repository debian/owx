/* $Id$ */

#pragma once

#include <string>
#include "comm.h"

class CWouxun
{
private:
	std::string m_port;
	unsigned m_timeout;
	std::string m_id;
	CComm m_comm;

	void RecvAck(bool checkecho = false);
	void PrepareAddress(char *buf, unsigned short addr);

public:
	CWouxun();

	void SetPort(const std::string &port);	// mandatory
	void SetTimeout(unsigned timeout);	// not mandatory

	void Open();
	const std::string &GetIDString() const;

	void GetPage(unsigned short addr, char *buf);		// 0x40-byte pages
	void PutPage(unsigned short addr, const char *buf);	// 0x10-byte pages
};
