/* $Id$ */

#pragma once

#include <string>
#include <vector>
#include <map>

class CCli
{
private:
	std::map<char, std::string> m_data;
	std::vector<std::string> m_rest;

public:
	CCli(int ac, char * const av[], const char *optstring);

	bool Avail(const char &key) const;
	const std::string &Param(const char &key) const;

	size_t RestSize() const;
	const std::string &Rest(size_t i) const;
};
