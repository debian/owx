/* $Id$ */

#pragma once

// 9600 8n1 is fixed

#include <sys/types.h>

class CComm
{
private:
	int m_fd;
	unsigned m_timeout;

public:
	CComm();
	~CComm();

	void Open(const std::string &dev, unsigned timeout);

	void Send(const void *data, size_t len);
	void Recv(void *data, size_t len);

	void SendChar(const char ch);
	char RecvChar();
};
