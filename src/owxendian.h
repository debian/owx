/* $Id$ */

#pragma once

#include <sys/types.h>
#include <sys/param.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
	#define OWX_LITTLE_ENDIAN
#elif __BYTE_ORDER == __BIG_ENDIAN
	#undef OWX_LITTLE_ENDIAN
#else
	#error Cannot determine your endianness, please contact author!
#endif
