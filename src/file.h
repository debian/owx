/* $Id$ */

#pragma once

#include <string>
#include <cstdio>

class CFileBase
{
protected:
	std::string m_path;
	FILE *m_fp;

public:
	CFileBase();
	virtual ~CFileBase();

	virtual void Open(const char *path) = 0;
	void Close();
};

class CFileRead: public CFileBase
{
private:
	size_t m_size;

public:
	~CFileRead();

	virtual void Open(const char *path);
	size_t Size() const;
	size_t Read(void *buf, size_t sz);
};

class CFileWrite: public CFileBase
{
public:
	~CFileWrite();

	virtual void Open(const char *path);
	void Write(const void *buf, size_t sz);
};
