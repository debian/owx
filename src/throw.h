/* $Id$ */

#pragma once

void __attribute__((noreturn)) Throw(const char *fmt, ...);
