/* $Id$ */

#pragma once

#include "cli.h"

void CmdUnknown(const CCli &cli);
void CmdCheck(const CCli &cli);
void CmdGet(const CCli &cli);
void CmdPut(const CCli &cli);
void CmdImport(const CCli &cli);
void CmdExport(const CCli &cli);
