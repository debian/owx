/* $Id$ */

#include <cstdio>
#include "util.h"
#include "throw.h"
#include "intl.h"

std::string Util::Sanitize(const std::string s)
{
	std::string out;

	for (std::string::const_iterator i(s.begin()); i != s.end(); ++i)
	{
		if (*i == '"')
		{
			out += std::string("\"\"");
			continue;
		}

		if (*i >= 0x20 && *i < 0x7F)
		{
			out += std::string(1, *i);
			continue;
		}

		char buf[5];
		snprintf(buf, sizeof(buf), "\\x%02X", (unsigned char) *i);
		out += buf;
	}

	return out;
}

std::string Util::IntToStr(size_t i)
{
	char buf[8];
	snprintf(buf, sizeof(buf), "%u", i);
	return buf;
}

std::string Util::NextToken(std::string &s, const char sep)
{
	size_t f(s.find(sep));
	if (f == s.npos)
		f = s.size();

	std::string rs(s.substr(0, f));
	s.erase(0, f + 1);
	return rs;
}

unsigned char Util::FromHexOne(const char ch)
{
	if (ch >= '0' && ch <= '9')
		return ch - '0';

	if (ch >= 'A' && ch <= 'F')
		return ch - 'A' + 10;

	if (ch >= 'a' && ch <= 'f')
		return ch - 'a' + 10;

	Throw(_("Invalid hex char %02X"), ch);
}

unsigned char Util::FromHex(const std::string &s)
{
	return (FromHexOne(s[0]) << 4) | FromHexOne(s[1]);
}

std::string Util::StripDot(const std::string &s)
{
	std::string rs;
	for (std::string::const_iterator i(s.begin()); i != s.end(); ++i)
		if (*i != '.')
			rs += std::string(1, *i);
	return rs;
}
