/* $Id$ */

#pragma once

#include "csv.h"

#define ROWS 147
#define COLS 10

void Import(char *buf, size_t bufsz, const CCsv &csv);
void Export(CCsv &csv, const char *buf, size_t bufsz);
