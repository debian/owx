/* $Id$ */

#pragma once

#include <string>

namespace Util
{
	std::string Sanitize(const std::string s);
	std::string IntToStr(size_t i);
	std::string NextToken(std::string &s, const char sep);
	unsigned char FromHexOne(const char ch);
	unsigned char FromHex(const std::string &s);
	std::string StripDot(const std::string &s);
};
