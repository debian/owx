/* $Id$ */

#ifndef _GNU_SOURCE
#	define _GNU_SOURCE
#endif

#include <stdexcept>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cerrno>
#include "throw.h"
#include "intl.h"

void __attribute__((noreturn)) Throw(const char *fmt, ...)
{
	char *p;
	va_list ap;

	va_start(ap, fmt);
	int rs(vasprintf(&p, fmt, ap));
	va_end(ap);

	if (rs == -1)
		throw std::logic_error(_("Not enough memory to format error string"));

	std::string s(p);
	free(p);

	throw std::logic_error(s);
}
