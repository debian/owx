/* $Id$ */

#pragma once

#include <vector>
#include <string>

class CCsv
{
private:
	unsigned m_rows, m_cols;
	std::vector<std::string> m_data;

	size_t Index(unsigned row, unsigned col) const;

	void Explode(std::string s);
	std::string Implode();

public:
	CCsv(unsigned rows, unsigned cols);

	void Load(const std::string &path);
	void Save(const std::string &path);

	const std::string &Read(unsigned row, unsigned col) const;
	void Write(unsigned row, unsigned col, const std::string &s);
};
