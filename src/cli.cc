/* $Id$ */

#include <unistd.h>
#include "throw.h"
#include "intl.h"
#include "cli.h"

CCli::CCli(int ac, char * const av[], const char *optstring)
{
	opterr = 0;

	for (;;)
	{
		int rs(getopt(ac, av, optstring));

		if (rs == -1)
			break;

		if (rs == '?')
			Throw(_("CLI: -%c: Invalid option"), optopt);

		if (rs == ':')
			Throw(_("CLI: -%c: Missing argument"), optopt);

		m_data[rs] = (optarg && *optarg) ? optarg : "";
	}

	for (int i(optind); i < ac; ++i)
		m_rest.push_back(av[i]);
}

bool CCli::Avail(const char &key) const
{
	return m_data.find(key) != m_data.end();
}

const std::string &CCli::Param(const char &key) const
{
	return m_data.find(key)->second;
}

size_t CCli::RestSize() const
{
	return m_rest.size();
}

const std::string &CCli::Rest(size_t i) const
{
	return m_rest[i];
}
